MediaWiki for Debian
--------------------
Installing:
	MediaWiki requires one of the following databases: MariaDB/MySQL
	(preferred), SQLite, or PostgreSQL.
	.
	Visit <http://localhost/mediawiki/> in your browser to go through
	MediaWiki's web installer. Save the resulting LocalSettings.php file
	in /etc/mediawiki. Your wiki should now be accessible.

Customizing:
	If you wish to customize MediaWiki after installation, you can do
	so. <https://www.mediawiki.org/wiki/Manual:System_administration>
	has details on how to go about that.
	For a better experience, setting up a background job processor is
	recommended, and a systemd service unit is provided for convenience.
	Run $ systemctl enable mediawiki-jobrunner; to start it.

Upgrading:
	Upgrading to a new major MediaWiki version usually has associated
	database schema updates. These will need to be applied, and can be
	done by visiting <http://localhost/mediawiki/mw-config/> in your
	browser, or by running the "update.php" command-line script
	(requires the php-cli package). It is recommended to back up your
	database before upgrading, just in case.

Editing:
	For help on editing, see <https://www.mediawiki.org/wiki/Help:Editing>.

Filesystem:
	MediaWiki is typically deployed in one complete directory, however
	we have split it up into multiple directories to adhere with the FHS:
	* /usr/share/mediawiki - MediaWiki core files
	* /etc/mediawiki/LocalSettings.php - expected location of
	  LocalSettings.php. You can also put other configuration files in this
	  directory.
	* /var/lib/mediawiki - Main directory that the webserver will read from.
	  Most core files from /usr/share/mediawiki are symlinked into this
	  directory, and the LocalSettings.php file as well.
	* /var/lib/mediawiki/extensions - Extensions are read from this directory.
	  Non-packaged extensions can be installed here as well.

Contributing:
	Help is welcome in maintaining the mediawiki package! The git
	repository is <https://salsa.debian.org/mediawiki-team/mediawiki>,
	patches can be sent via Salsa, through the Debian bug tracker, or
	via e-mail.
